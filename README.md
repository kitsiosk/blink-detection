Python program that counts the number of blinks in real time video capture.

The pretrained dlib model can be downloaded here: http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2